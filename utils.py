from functools import wraps
from time import time


def print_green(txt):
    """
    Text verde
    :param txt: string
    :return: -
    """
    okgreen = '\033[92m'
    bold = "\033[1m"
    ENDC = '\033[0m'
    print(okgreen + bold + txt + ENDC)


def timing(func):
    """
    Decorator 4 timing
    :param func:
    :return:
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        start = time()
        result = func(*args, **kwargs)
        end = time()
        print_green(func.__name__ + " takes: " + str(end - start) + " seconds.")
        return result
    return wrapper
