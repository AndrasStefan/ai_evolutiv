from collections import namedtuple
from random import randrange, choice

from utils import timing, print_green

DIMENSION_OF_CHROMOSOME = None
SELECTION_PARAMS = None
PROBABILITY_CROSSOVER = None
PROBABILITY_MUTATION = None

Chromosome = namedtuple('Chromosome', 'position fitness')


def comparator(chromo):
    """
    comparator pentru Chromosome
    :param chromo:
    :return:
    """
    return chromo.fitness


def read_from_file_onedim(path):
    """
    Read the length, and an array from file
    :param path:
    :return:
    """
    with open(path) as file:
        dim = int(file.readline())
        rez = list(map(float, file.readline().split()))
        return rez, dim


def read_from_file_alldim(path):
    """
    Read mutiple dim and return one array and cumulated length
    :param path:
    :return:
    """
    with open(path) as file:
        last_line = ""
        try:
            last_line = file.readline()
            int(last_line)
            # dims = [int(last_line)]
            while True:
                last_line = file.readline()
                int(last_line)
                # dims.append(int(last_line))
        except ValueError:
            rez = list(map(float, last_line.split()))
            for line in file:
                rez += list(map(float, line.split()))

        return rez, len(rez)


INPUT_FILE = "input/no_dims.txt"
numbers, n = read_from_file_alldim(INPUT_FILE)  # use them as globals
POPULATION_SIZE = n * 2
NUMBER_OF_GENERATIONS = n // 5
MUTATION_CONSTANT = n // 3  # // 4 for duplicates


def solve():
    """
    Create new generations and choose the best from there
    :return: best from last generation, surely best
    """
    population = initialization()
    population = fitness(population)

    for gen in range(NUMBER_OF_GENERATIONS):
        pop_aux = []
        for i in range(POPULATION_SIZE):
            m = selection(population)
            f = selection(population)
            new = mutation(crossover(m, f))
            pop_aux.append(new)
        population = pop_aux
        population = fitness(population)

        print("Generatia: " + str(gen + 1) + " " + str(population))

    best_from_ea = best(population)
    surely_best = best_normal_way()
    print("Best from EAs: " + str(best_from_ea))
    print("Best: " + str(surely_best))

    return best_from_ea, surely_best


def fitness(pop):
    """
    Compute the fitness for every element of a population
    :param pop:
    :return:
    """
    return [Chromosome(chromo.position, numbers[chromo.position]) for chromo in pop]


def initialization():
    """
    Initialization for a population
    :return:
    """
    return [Chromosome(randrange(n), 0.0) for _ in range(POPULATION_SIZE)]


def selection(pop):
    """
    Select 2 parents from poulation (binar tournament)
    :param pop:
    :return:
    """
    m, f = choice(pop), choice(pop)
    if m.fitness < f.fitness:
        return m
    return f


def crossover(m, f):
    """
    Creating a new chromosome from 2 parents
    :param m:
    :param f:
    :return:
    """
    new_pos = (m.position + f.position) // 2
    if 0 <= new_pos < n:
        return Chromosome(new_pos, 0.0)
    return Chromosome(m.position, 0.0)


def mutation(chromo, e=MUTATION_CONSTANT):
    """
    Change one chromosom with a constant
    :param chromo:
    :param e:
    :return:
    """
    sign = choice('-+')
    if sign == '+':
        new_pos = chromo.position + e
    else:
        new_pos = chromo.position - e

    if 0 <= new_pos < n:
        return Chromosome(new_pos, 0.0)
    return chromo


def best(pop):
    """
    Choose the best chromosome from a population
    :param pop:
    :return:
    """
    curent_min = min(pop, key=comparator)
    # curent_min = min(pop, key=lambda x: x.fitness)
    return {chromo for chromo in pop if chromo.fitness == curent_min.fitness}


def best_normal_way():
    """
    Compute all the minims from an array, and their positions
    :return:
    """
    mins = []
    curent_min = 10000
    for i, number in enumerate(numbers):
        if number == curent_min:
            mins.append((i, number))
        elif number < curent_min:
            mins = [(i, number)]
            curent_min = number

    return mins


@timing
def multiple_runs(number):
    """
    Solve one input multiple times, and check the answear
    :param number:
    :return:
    """
    #
    counter = 0
    for _ in range(number):
        best_ea, best_surely = solve()
        # if best_ea.pop() == best_surely[0]: # work with no duplicates only
        if best_ea == set(best_surely):  # DONE: make this run with multiple mins
            counter += 1
    print_green("Results: " + str(counter) + " / " + str(number))


def test_read_from_file_alldim():
    """
    Parsing the file test
    :return:
    """
    rez, dim = read_from_file_alldim("input/02_date1.txt")
    assert rez == [1.81971, 1.55698, 0.745278, -0.538449, -2.07263, -3.52929, -4.54225, -4.79134, -4.08441, -2.41789]
    assert dim == 10

    rez, dim = read_from_file_alldim("input/no_dims.txt")
    assert rez == [-2.07263, 1.55698, -2.07263, -0.538449, -2.07263, 1.55698, 1.55698, 1.55698, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0, 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0]
    assert dim == 33


if __name__ == "__main__":
    multiple_runs(10)
    # print(numbers)
